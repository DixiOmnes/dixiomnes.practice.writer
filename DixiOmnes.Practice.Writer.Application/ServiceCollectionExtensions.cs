﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace DixiOmnes.Practice.Writer.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<ISortedArrayService, SortedArrayService>();

            return services;
        }
    }
}
