﻿using DixiOmnes.Practice.Writer.Domain;
using System.Threading.Tasks;

namespace DixiOmnes.Practice.Writer.Application
{
    public interface ISortedArrayService
    {
        Task Write(ISortedArray sortedArray);
    }
}