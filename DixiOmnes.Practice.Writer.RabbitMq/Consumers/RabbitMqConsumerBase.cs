﻿using DixiOmnes.Practice.Writer.RabbitMq.Configuration;
using DixiOmnes.Practice.Writer.RabbitMq.Contracts;
using DixiOmnes.Practice.Writer.RabbitMq.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.Practice.Writer.RabbitMq.Consumers
{
    internal abstract class RabbitConsumerBase<TMessage> : IDisposable
        where TMessage : IRabbitMQMessage
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;
        private IConnection _connection;
        private IModel _channel;

        public string ConsumingQueue { get; set; }
        public string ParkingQueue { get; set; }

        public RabbitConsumerBase(IOptions<RabbitMQConsumeConfig> options,
                                  ILogger logger,
                                  IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));

            var credentials = options.Value.Credentials;

            var connectionFactory = new ConnectionFactory()
            {
                HostName = credentials.HostName,
                UserName = credentials.UserName,
                Password = credentials.Password,
                VirtualHost = credentials.VirtualHost,
                Port = AmqpTcpEndpoint.UseDefaultPort,
                AutomaticRecoveryEnabled = true
            };

            _connection = connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();

            ConsumingQueue = options.Value.ConsumingQueue;
            ParkingQueue = options.Value.ParkingQueue;
        }

        public void Subscribe<THandler>() where THandler : IEventHandler<TMessage>
        {
            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += async (model, ea) =>
            {
                try
                {
                    await RetryPolicyFactory.CreateAsync().ExecuteAsync(async () =>
                    {
                        var str = Encoding.UTF8.GetString(ea.Body);
                        var message = JsonConvert.DeserializeObject<TMessage>(str);

                        using var scope = _serviceProvider.CreateScope();
                        var handler = scope.ServiceProvider.GetRequiredService<THandler>();
                        await handler.HandleAsync(message);
                    });
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    await Park(ea);
                }

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume(ConsumingQueue, false, consumer);
        }

        private Task Park(BasicDeliverEventArgs ea)
        {
            RetryPolicyFactory.Create().Execute(() =>
            {
                _channel.BasicPublish(exchange: string.Empty,
                    ParkingQueue,
                    null,
                    ea.Body);

                _logger.LogDebug($"Message parked.");
            });

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _channel?.Close();
            _connection?.Close();

            _connection?.Dispose();
            _channel?.Dispose();
        }

    }
}
