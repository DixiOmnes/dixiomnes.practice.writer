﻿using DixiOmnes.Practice.Writer.RabbitMq.Configuration;
using DixiOmnes.Practice.Writer.RabbitMq.Contracts;
using DixiOmnes.Practice.Writer.RabbitMq.Contracts.Messages;
using DixiOmnes.Practice.Writer.RabbitMq.Handlers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DixiOmnes.Practice.Writer.RabbitMq.Consumers
{
    class SortedArrayConsumer : RabbitConsumerBase<SortedArrayMessage>, ISortedArrayConsumer
    {
        public SortedArrayConsumer(IOptions<RabbitMQConsumeConfig> options,
                                   ILogger logger,
                                   IServiceProvider serviceProvider) : base(options, logger, serviceProvider)
        {
        }

        public void BindToQueue()
        {
            Subscribe<SortedArrayEventHandler>();
        }
    }
}
