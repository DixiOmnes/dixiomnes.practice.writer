﻿using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Text;

namespace DixiOmnes.Practice.Writer.RabbitMq.Helpers
{
    internal static class RetryPolicyFactory
    {
        public static RetryPolicy Create()
            => Policy.Handle<Exception>()
            .WaitAndRetry(3, retryAttemptNum => TimeSpan.FromSeconds(Math.Pow(2, retryAttemptNum)));

        public static AsyncRetryPolicy CreateAsync()
            => Policy.Handle<Exception>()
            .WaitAndRetryAsync(3, retryAttemptNum => TimeSpan.FromSeconds(Math.Pow(2, retryAttemptNum)));
    }
}
