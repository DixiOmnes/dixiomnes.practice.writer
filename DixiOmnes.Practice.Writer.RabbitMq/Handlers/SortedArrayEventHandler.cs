﻿using DixiOmnes.Practice.Writer.Application;
using DixiOmnes.Practice.Writer.Domain;
using DixiOmnes.Practice.Writer.RabbitMq.Contracts;
using DixiOmnes.Practice.Writer.RabbitMq.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.Practice.Writer.RabbitMq.Handlers
{
    class SortedArrayEventHandler : IEventHandler<SortedArrayMessage>
    {
        private readonly ISortedArrayService _sortedArrayService;

        public SortedArrayEventHandler(ISortedArrayService sortedArrayService)
        {
            _sortedArrayService = sortedArrayService ?? throw new ArgumentNullException(nameof(sortedArrayService));
        }

        public async Task HandleAsync(SortedArrayMessage message)
        {
            var sortedArray = SortedArray.Create(message.Array, message.Created, message.Sorted);

            await _sortedArrayService.Write(sortedArray);
        }
    }
}
