﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DixiOmnes.Practice.Writer.RabbitMq.Configuration
{
    public class RabbitMQConsumeConfig
    {
        public RabbitMQCredentials Credentials { get; set; }
        public string ConsumingQueue { get; set; }
        public string ParkingQueue { get; set; }
    }
}
