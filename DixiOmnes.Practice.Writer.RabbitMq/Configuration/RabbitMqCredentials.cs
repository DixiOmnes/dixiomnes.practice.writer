﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DixiOmnes.Practice.Writer.RabbitMq.Configuration
{
    public class RabbitMQCredentials
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; }
    }
}
