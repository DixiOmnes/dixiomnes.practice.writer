﻿namespace DixiOmnes.Practice.Writer.RabbitMq.Contracts
{
    internal interface ISortedArrayConsumer
    {
        void BindToQueue();
    }
}