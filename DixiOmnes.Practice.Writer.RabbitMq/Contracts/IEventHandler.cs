﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.Practice.Writer.RabbitMq.Contracts
{
    internal interface IEventHandler<in TMessage> where TMessage : IRabbitMQMessage
    {
        Task HandleAsync(TMessage message);
    }
}
