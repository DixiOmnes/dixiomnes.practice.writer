﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DixiOmnes.Practice.Writer.RabbitMq.Contracts.Messages
{
    class SortedArrayMessage : IRabbitMQMessage
    {
        public int[] Array { get; set; }
        public DateTime Created { get; set; }
        public DateTime Sorted { get; set; }
    }
}
