﻿using DixiOmnes.Practice.Writer.RabbitMq.Consumers;
using DixiOmnes.Practice.Writer.RabbitMq.Contracts;
using DixiOmnes.Practice.Writer.RabbitMq.Handlers;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DixiOmnes.Practice.Writer.RabbitMq
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRabbitMqServices(this IServiceCollection services)
        {
            services.AddSingleton<ISortedArrayConsumer, SortedArrayConsumer>();
            services.AddTransient<SortedArrayEventHandler>();

            return services;
        }
    }
}
