﻿using System;

namespace DixiOmnes.Practice.Writer.Domain
{
    public interface ISortedArray
    {
        DateTime Sorted { get; }

        int[] GetArray();
    }
}