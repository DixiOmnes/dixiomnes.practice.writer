﻿using System;

namespace DixiOmnes.Practice.Writer.Domain
{
    public class SortedArray : ISortedArray
    {
        private int[] _array;
        public DateTime Created { get; private set; }
        public DateTime Sorted { get; private set; }

        private SortedArray(int[] unorderedArray, DateTime created, DateTime sorted)
        {
            if (unorderedArray is null)
                throw new ArgumentNullException(nameof(unorderedArray));

            if (unorderedArray.Length == 0 || unorderedArray.Length > 10)
                throw new ArgumentException(nameof(unorderedArray));

            _array = new int[unorderedArray.Length];
            unorderedArray.CopyTo(_array, 0);
            Array.Sort(_array);

            Created = created;
            Sorted = sorted;
        }        

        
        public static ISortedArray Create(int[] array, DateTime created, DateTime sorted)
        {
            return new SortedArray(array, created, sorted);
        }

        public static ISortedArray Create(int[] array, DateTime created)
        {
            return new SortedArray(array, created, DateTime.Now);
        }

        public static ISortedArray Create(int[] array)
        {
            return new SortedArray(array, DateTime.Now, DateTime.Now);
        }

        public int[] GetArray()
        {
            return _array.Clone() as int[];
        }
    }
}
