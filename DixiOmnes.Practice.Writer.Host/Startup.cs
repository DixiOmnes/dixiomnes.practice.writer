using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DixiOmnes.Practice.Writer.Application;
using DixiOmnes.Practice.Writer.RabbitMq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DixiOmnes.Practice.Writer.Host
{
    public class Startup
    {
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRabbitMqServices();
            services.AddServices();
        }

        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
        }
    }
}
